package main

import (
	"test-service/internal/pkg"

	_ "test-service/docs"
	"test-service/internal/config"
	"test-service/internal/handler"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	fibSwag "github.com/swaggo/fiber-swagger"
)

// @title Test - Back End
// @version 1.0
// @description This Watch service sandbox is used for testing purpose only.
// @termsOfService http://swagger.io/terms/

// @contact.name Yoopy Christian
// @contact.url https://www.linkedin.com/in/yoopychristian/
// @contact.email yoopychs@gmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /api/test
func main() {
	app := fiber.New()
	base := config.NewBase()
	hcfg := config.HandlerConfig{Logger: base.Logger}

	//load infra things
	pool := pkg.LoadPool(base.Logger)
	redis := pkg.LoadRedis(base.Logger)
	env := pkg.LoadConfig()

	//load business container
	dao := pkg.RegisterRepository(base, pool)
	uc := pkg.RegisterUsecase(base, dao, redis)

	//load handler
	mAuth := config.HandlerAuth(config.Middleware{
		Logger: base.Logger,
	})
	app.Use(config.HandlerLog(hcfg), cors.New())
	app.Get(env.ContextPath+"/swagger/*", fibSwag.WrapHandler)
	handler.DefaultHandler(app, env.ContextPath)

	m := app.Group("/api/test/v1").Use(mAuth)
	handler.ProductHandler(m, handler.Product{ProductManagement: *uc.ProductManagement})
	handler.BrandHandler(m, handler.Brand{BrandManagement: *uc.BrandManagement})
	handler.OrderHandler(m, handler.Order{OrderManagement: *uc.OrderManagement})
	n := app.Group("/api/test")
	handler.UserHandler(n, handler.User{})

	_ = app.Listen(env.HttpPort)
}
