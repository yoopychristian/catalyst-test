package domain

import (
	"database/sql"

	"github.com/shopspring/decimal"
)

type (
	Order struct {
		Id         sql.NullInt64       `json:"id"`
		BrandId    sql.NullInt64       `json:"brand_id"`
		ProductId  sql.NullInt64       `json:"product_id"`
		Qty        sql.NullInt64       `json:"qty"`
		TotalPrice decimal.NullDecimal `json:"total_price"`
		Status     sql.NullInt32       `json:"status"`
		Notes      sql.NullString      `json:"notes"`
		BaseEntity
	}
)

type (
	AddOrderRequest struct {
		ProductId int64  `json:"product_id" form:"product_id" binding:"required" example:"1"`
		Qty       int64  `json:"qty" form:"qty" binding:"required" example:"1"`
		Notes     string `json:"notes" form:"forms" binding:"required" example:"Tolong yang mulus"`
		SessionRequest
	}

	AddCategoriesOrderRequest struct {
		ParameterId int64  `json:"parameter_id" form:"parameter_id" binding:"required" example:"1"`
		Value       string `json:"value" form:"value" binding:"required" example:"Jam Tangan"`
	}
)

type (
	OrderSearchResponse struct {
		Id        int64   `json:"id"`
		OrderName string  `json:"order"`
		ShortDesc float64 `json:"short_desc"`
		Status    int     `json:"status"`
	}

	OrderResponses struct {
		Id          int64  `json:"id,omitempty"`
		ProductName string `json:"product_name,omitempty"`
		Qty         int    `json:"qty,omitempty"`
		TotalPrice  int64  `json:"total_price,omitempty"`
		Notes       string `json:"notes,omitempty"`
	}

	OrderCategoriesResponse struct {
		ParameterId int16  `json:"parameter_id,omitempty"`
		Value       string `json:"value,omitempty"`
	}

	OrderDetailProjection struct {
		Orders OrderResponses `json:"orders,omitempty"`
	}

	OrderSearchProjection struct {
		Id     int64  `json:"id" db:"id"`
		Order  string `json:"order" db:"order"`
		Status int    `json:"status" db:"status"`
	}

	OrdersSearchResponse struct {
		Orders []OrderSearchResponse `json:"orders,omitempty"`
		PaginationResponse
	}

	OrderList struct {
		Id       int64  `json:"id"`
		Order    string `json:"Order"`
		Category string `json:"category"`
	}
)
