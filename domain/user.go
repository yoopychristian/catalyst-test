package domain

type SignupRequest struct {
	Username string `json:"username" form:"username" binding:"required" example:"alex123"`
	Fullname string `json:"fullname" form:"fullname" binding:"required" example:"alexander graham"`
	Email    string `json:"email" form:"email" binding:"required" example:"alex@gmail.com"`
	Password string `json:"password" form:"password" binding:"required" example:"-"`
}

type LoginRequest struct {
	Email    string `json:"email" form:"email" binding:"required" example:"alex@gmail.com"`
	Password string `json:"password" form:"password" binding:"required" example:"-"`
}

type User struct {
	Id       int64
	Username string
	Fullname string
	Email    string
	Password string `json:"-"`
}
