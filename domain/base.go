package domain

import (
	"database/sql"
	"test-service/internal"
	"time"
)

type BaseEntity struct {
	IsDeleted   bool           `json:"is_deleted"`
	CreatedBy   sql.NullString `json:"created_by"`
	CreatedDate sql.NullTime   `json:"created_date"`
	UpdatedBy   sql.NullString `json:"updated_by"`
	UpdatedDate sql.NullTime   `json:"updated_date"`
}

type BusinessError struct {
	ErrorCode    string
	ErrorMessage string
}

type TechnicalError struct {
	Exception string `json:"exception"`
	Occurred  int64  `json:"occurred_time"`
	Ticket    string `json:"ticket"`
}

type ContextLog struct {
	Api                  string
	MediaType            string
	Method               string
	ClientReqTime        time.Time
	IncomingReqTime      int64
	HttpStatus           int
	OutgoingResponseTime int64
	ErrorResponse        interface{}
}

type (
	Response struct {
		Data interface{} `json:"data,omitempty"`
		Meta Meta        `json:"meta,omitempty"`
	}

	Meta struct {
		Code    string `json:"code,omitempty"`
		Message string `json:"message,omitempty"`
	}
)

type (
	PaginationResponse struct {
		Number        int    `json:"number,omitempty"`
		Size          int    `json:"size,omitempty"`
		TotalElements int    `json:"total_elements,omitempty"`
		TotalPages    int    `json:"total_pages,omitempty"`
		Sort          string `json:"sort,omitempty"`
		SortBy        string `json:"sort_by,omitempty"`
	}

	ValidationResponse struct {
		Result bool `json:"result"`
	}

	SessionResponse struct {
		Username    string   `json:"username"`
		Msisdn      string   `json:"msisdn"`
		Role        string   `json:"role"`
		Id          string   `json:"id"`
		Email       string   `json:"email"`
		Fullname    string   `json:"fullname"`
		Expired     uint64   `json:"expired"`
		Status      int      `json:"status"`
		Permissions []string `json:"permissions"`
		AccessToken string   `json:"accessToken"`
	}
)

type (
	DeleteRequest struct {
		Id           uint   `json:"id"`
		LoggedUserId uint   `json:"loggedUserId"`
		LoggedUser   string `json:"loggedUser"`
	}

	FindByIdRequest struct {
		Id uint `json:"id"`
		SessionRequest
	}

	SearchRequest struct {
		TextSearch string `json:"text_search"`
		Start      uint   `json:"start" binding:"required" example:"0"`
		Limit      uint   `json:"limit" binding:"required" example:"5"`
		SortBy     string `json:"sort_by"`
		Sort       string `json:"sort"`
		SessionRequest
	}

	SessionRequest struct {
		Username string `swaggerignore:"true"`
		Msisdn   string `swaggerignore:"true"`
		Role     string `swaggerignore:"true"`
		Id       string `swaggerignore:"true"`
		Email    string `swaggerignore:"true"`
		Fullname string `swaggerignore:"true"`
		ContextRequest
	}

	TransactionResponse struct {
		TransactionTimestamp uint   `json:"transaction_timestamp"`
		TransactionId        string `json:"transaction_id"`
	}

	TransactionResponseWithId struct {
		Id                   int64  `json:"id"`
		TransactionTimestamp uint   `json:"transaction_timestamp"`
		TransactionId        string `json:"transaction_id"`
	}

	TransactionResponseWithIdPrice struct {
		Id                   int64  `json:"id"`
		Price                int64  `json:"price"`
		TransactionTimestamp uint   `json:"transaction_timestamp"`
		TransactionId        string `json:"transaction_id"`
	}

	TransactionResponseAuthorization struct {
		Token                string `json:"token"`
		TransactionTimestamp uint   `json:"transaction_timestamp"`
		TransactionId        string `json:"transaction_id"`
	}

	ContextRequest struct {
		Channel       string `json:"channel" swaggerignore:"true"`
		OS            string `json:"os" swaggerignore:"true"`
		Version       string `json:"version" swaggerignore:"true"`
		Language      string `json:"language" swaggerignore:"true"`
		DeviceId      string `json:"deviceId" swaggerignore:"true"`
		Authorization string `json:"authorization" swaggerignore:"true"`
		RefreshToken  string `json:"refreshToken" swaggerignore:"true"`
		TransactionId string `json:"transactionId" swaggerignore:"true"`
		ApiKey        string `json:"apiKey" swaggerignore:"true"`
	}
)

func BusinessErrorResponse(e *BusinessError) *Response {
	return &Response{
		Meta: Meta{Code: e.ErrorCode, Message: e.ErrorMessage},
		Data: nil,
	}
}

func BadPayloadMeta() Meta {
	return Meta{
		Code:    internal.ErrCodeBadPayload,
		Message: internal.ErrMsgBadPayload,
	}
}

func TechnicalErrorResponse(e *TechnicalError) *Response {
	return &Response{
		Meta: Meta{Code: internal.ErrCodeSomethingWrong, Message: internal.ErrMsgSomethingWrong},
		Data: e,
	}
}

func DefaultSuccessResponse(m string, data interface{}) *Response {
	return &Response{
		Meta: Meta{Code: internal.SuccessCode, Message: m},
		Data: data,
	}
}

func CustomResponse(c string, m string, data interface{}) *Response {
	return &Response{
		Meta: Meta{Code: c, Message: m},
		Data: data,
	}
}
