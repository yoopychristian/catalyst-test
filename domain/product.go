package domain

import (
	"database/sql"

	"github.com/shopspring/decimal"
)

type (
	Product struct {
		Id            sql.NullInt64       `json:"id"`
		BrandId       sql.NullInt64       `json:"brand_id"`
		ProductName   sql.NullString      `json:"product_name"`
		PromoStatus   sql.NullBool        `json:"promo_status"`
		OriginalPrice decimal.NullDecimal `json:"original_price"`
		ShortDesc     sql.NullString      `json:"short_desc"`
		Status        sql.NullInt32       `json:"status"`
		BaseEntity
	}
)

type (
	AddProductRequest struct {
		BrandId       int64  `json:"brand_id" form:"brand_id" binding:"required" example:"1"`
		ProductName   string `json:"product_name" form:"product_name" binding:"required" example:"Gshock"`
		OriginalPrice int64  `json:"original_price" form:"original_price" binding:"required" example:"1800000"`
		ShortDesc     string `json:"short_desc" form:"short_desc" binding:"required" example:"Gshock adalah jam tangan merk Casio"`
		SessionRequest
	}

	GetQueryRequest struct {
		Id string `json:"id" form:"id"`
		SessionRequest
	}
)

type (
	ProductBrandResponse struct {
		Id            int64   `json:"id"`
		BrandName     string  `json:"brand_name"`
		ProductName   string  `json:"product_name"`
		PromoStatus   bool    `json:"promo_status"`
		OriginalPrice float64 `json:"original_price"`
		ShortDesc     string  `json:"short_desc"`
		Status        int     `json:"status"`
	}

	ProductsDetailProjection struct {
		Products []ProductBrandResponse `json:"products,omitempty"`
	}

	ProductDetailProjection struct {
		Product ProductBrandResponse `json:"product,omitempty"`
	}
)
