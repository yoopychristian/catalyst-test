package domain

import (
	"database/sql"
)

type (
	Brand struct {
		Id        sql.NullInt64  `json:"id"`
		Brand     sql.NullString `json:"brand"`
		ShortDesc sql.NullString `json:"short_desc"`
		Status    sql.NullInt32  `json:"status"`
		BaseEntity
	}

	BrandCategory struct {
		BrandId     sql.NullInt64  `json:"brand_id"`
		UserId      sql.NullInt64  `json:"user_id"`
		ParameterId sql.NullInt64  `json:"parameter_id"`
		Value       sql.NullString `json:"value"`
		BaseEntity
	}
)

type (
	AddBrandRequest struct {
		Brand     string `json:"brand" form:"brand" binding:"required" example:"Casio"`
		ShortDesc string `json:"short_desc" form:"short_desc" binding:"required" example:"Casio adalah jam tangan yang sangat bagus..."`
		SessionRequest
	}

	AddCategoriesBrandRequest struct {
		ParameterId int64  `json:"parameter_id" form:"parameter_id" binding:"required" example:"1"`
		Value       string `json:"value" form:"value" binding:"required" example:"Jam Tangan"`
	}
)

type (
	BrandSearchResponse struct {
		Id        int64   `json:"id"`
		BrandName string  `json:"brand"`
		ShortDesc float64 `json:"short_desc"`
		Status    int     `json:"status"`
	}

	BrandResponses struct {
		Id            int64   `json:"id,omitempty"`
		OriginalPrice float64 `json:"original_price"`
		ShortDesc     string  `json:"short_desc"`
	}

	BrandCategoriesResponse struct {
		ParameterId int16  `json:"parameter_id,omitempty"`
		Value       string `json:"value,omitempty"`
	}

	BrandDetailProjection struct {
		Brand           BrandResponses            `json:"brand,omitempty"`
		BrandCategories []BrandCategoriesResponse `json:"brand_categories,omitempty"`
	}

	BrandSearchProjection struct {
		Id     int64  `json:"id" db:"id"`
		Brand  string `json:"brand" db:"brand"`
		Status int    `json:"status" db:"status"`
	}

	BrandsSearchResponse struct {
		Brands []BrandSearchResponse `json:"brands,omitempty"`
		PaginationResponse
	}

	BrandList struct {
		Id       int64  `json:"id"`
		Brand    string `json:"brand"`
		Category string `json:"category"`
	}
)
