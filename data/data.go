package data

import (
	"fmt"
	"test-service/domain"

	"xorm.io/xorm"
)

func CreateDBEngine() (*xorm.Engine, error) {
	connectionInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", "localhost", 5432, "postgres", "admin", "postgres")
	engine, err := xorm.NewEngine("postgres", connectionInfo)
	if err != nil {
		return nil, err
	}
	if err := engine.Ping(); err != nil {
		return nil, err
	}
	if err := engine.Sync(new(domain.User)); err != nil {
		return nil, err
	}

	return engine, nil
}
