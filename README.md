# Catalyst TEST 

# Getting started
## Add Database Local

create table brands (id int8 not null, brand varchar(255), short_desc varchar(255), created_by varchar(255), created_date timestamp, status int, is_deleted bool, primary key (id));

create sequence brands_seq;
alter sequence brands_seq owner to postgres;

alter table brands alter column id set default nextval('public.brands_seq'::regclass);

create table products (id int8 not null, brand_id int8, product_name VARCHAR(255), promo_status bool, original_price decimal, short_desc VARCHAR(255), created_by varchar(255), created_date timestamp, status int, is_deleted bool);


create sequence products_seq;
alter sequence products_seq owner to postgres;

alter table products alter column id set default nextval('public.products_seq'::regclass);


create table orders (id int8 not null, brand_id int8, product_id int8, qty int, total_price decimal, notes VARCHAR(255), created_by varchar(255), created_date timestamp, status int, is_deleted bool);


create sequence orders_seq;
alter sequence orders_seq owner to postgres;

alter table orders alter column id set default nextval('public.orders_seq'::regclass);

## To run 

go run cmd/router.go

Open link local for swagger : http://127.0.0.1:8080/api/watch/swagger/index.html#/root/get_ping

## Example ENV

HTTP_PORT=":8080"
DB_HOST="localhost"
DB_PORT="5432"
DB_USER="postgres"
DB_SCHEMA="postgres"
DB_PASSWORD="admin"
DB_OPTIONS="pool_max_conns=50&pool_min_conns=1&pool_max_conn_lifetime=1&pool_max_conn_idle_time=2s"
REDIS_HOST="localhost:6379"
REDIS_PASSWORD=""
REDIS_DB_INDEX="0"
REDIS_MODE="SINGLE"
BASE_PATH="/api/watch"
TIME_DURATION_UPDATE="2h"