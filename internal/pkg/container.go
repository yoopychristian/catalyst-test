package pkg

import (
	"os"
	"strconv"
	"strings"

	"test-service/db"
	"test-service/internal/config"
	"test-service/internal/repository"
	"test-service/internal/usecase"

	"go.uber.org/zap"
)

type (
	Env struct {
		ContextPath string
		HttpPort    string
	}

	Infra struct {
		RestAdaptor config.RestAdaptor
	}

	Dao struct {
		Brand   *repository.Brand
		Product *repository.Product
		Order   *repository.Order
		User    *repository.User
	}

	Usecase struct {
		ProductManagement *usecase.ProductManagement
		UserManagement    *usecase.UserManagement
		BrandManagement   *usecase.BrandManagement
		OrderManagement   *usecase.OrderManagement
	}
)

func LoadPool(logger *zap.Logger) *db.PgPool {
	return db.NewPgPool(&db.PgOptions{
		Host:   os.Getenv("DB_HOST"),
		Port:   os.Getenv("DB_PORT"),
		User:   os.Getenv("DB_USER"),
		Passwd: os.Getenv("DB_PASSWORD"),
		Schema: os.Getenv("DB_SCHEMA"),
		Logger: logger,
	})
}

func LoadRedis(logger *zap.Logger) (rds db.Cacher) {
	addresses := strings.Split(os.Getenv("REDIS_HOST"), ";")
	password := os.Getenv("REDIS_PASSWORD")
	index, _ := strconv.Atoi(os.Getenv("REDIS_DB_INDEX"))
	rds = db.NewRedis(&db.RedisOptions{
		Addr:   addresses[0],
		Passwd: password,
		Index:  index,
		Logger: logger,
	})
	return rds
}

func RegisterRepository(b config.Base, p *db.PgPool) Dao {
	return Dao{
		Product: repository.NewProduct(repository.Product{Logger: b.Logger, PgPool: *p}),
		Brand:   repository.NewBrand(repository.Brand{Logger: b.Logger, PgPool: *p}),
		Order:   repository.NewOrder(repository.Order{Logger: b.Logger, PgPool: *p}),
		User:    repository.NewUser(repository.User{Logger: b.Logger}),
	}
}

func RegisterUsecase(b config.Base, d Dao, r db.Cacher) Usecase {
	return Usecase{
		ProductManagement: usecase.NewProductManagement(usecase.ProductManagement{
			Dao:    *d.Product,
			Logger: b.Logger,
		}),
		BrandManagement: usecase.NewBrandManagement(usecase.BrandManagement{
			Dao:    *d.Brand,
			Logger: b.Logger,
		}),
		OrderManagement: usecase.NewOrderManagement(usecase.OrderManagement{
			Dao:    *d.Order,
			Logger: b.Logger,
		}),
		UserManagement: usecase.NewUserManagement(usecase.UserManagement{
			Dao:          *d.User,
			Logger:       b.Logger,
			TimeDuration: os.Getenv("TIME_DURATION_UPDATE"),
		}),
	}
}

func LoadConfig() Env {
	return Env{
		ContextPath: os.Getenv("BASE_PATH"),
		HttpPort:    os.Getenv("HTTP_PORT"),
	}
}
