package handler

import (
	"test-service/domain"
	"test-service/internal"
	"test-service/internal/config"
	"test-service/internal/usecase"

	"github.com/gofiber/fiber/v2"
)

type Brand struct {
	usecase.BrandManagement
}

func newBrandResource(h Brand) *Brand {
	return &h
}

func BrandHandler(r fiber.Router, h Brand) {
	handler := newBrandResource(h)
	r.Post("/brand", handler.add)
}

// @Tags Brand Management APIs
// Add Brand API
// @Summary Add Brand API
// @Schemes
// @Accept json
// @Param Authorization header string true "Your Token to Access" default(Bearer )
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param request body domain.AddBrandRequest true "add Brand"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /v1/brand [post]
func (b *Brand) add(ctx *fiber.Ctx) error {
	inp := domain.AddBrandRequest{}
	if err := ctx.BodyParser(&inp); err != nil {
		return ctx.Status(404).JSON(err)
	}
	inp.SessionRequest = config.SessionBuild(ctx)
	v, ex := b.BrandManagement.Add(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgSubmit, v))
}
