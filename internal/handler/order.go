package handler

import (
	"test-service/domain"
	"test-service/internal"
	"test-service/internal/config"
	"test-service/internal/usecase"

	"github.com/gofiber/fiber/v2"
)

type Order struct {
	usecase.OrderManagement
}

func newOrderResource(h Order) *Order {
	return &h
}

func OrderHandler(r fiber.Router, h Order) {
	handler := newOrderResource(h)
	r.Post("/order", handler.add)
	r.Get("/order", handler.viewOrder)
}

// @Tags Order Management APIs
// Add Order API
// @Summary Add Order API
// @Schemes
// @Accept json
// @Param Authorization header string true "Your Token to Access" default(Bearer )
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param request body domain.AddOrderRequest true "add Order"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /v1/order [post]
func (o *Order) add(ctx *fiber.Ctx) error {
	inp := domain.AddOrderRequest{}
	if err := ctx.BodyParser(&inp); err != nil {
		return ctx.Status(404).JSON(err)
	}
	inp.SessionRequest = config.SessionBuild(ctx)
	v, ex := o.OrderManagement.Add(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgSubmit, v))
}

// @Tags Order Management APIs
// API Get Order
// @Summary Get Order API
// @Schemes
// @Accept json
// @Param Authorization header string true "Your Token to Access" default(Bearer )
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param Payload query domain.GetQueryRequest true "Query By Id Order"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /v1/order [get]
func (o *Order) viewOrder(ctx *fiber.Ctx) error {
	inp := domain.GetQueryRequest{}
	if err := ctx.QueryParser(&inp); err != nil {
		return ctx.Status(400).JSON(domain.BadPayloadMeta())
	}
	inp.SessionRequest = config.SessionBuild(ctx)
	inp.Id = ctx.Query("id")
	v, ex := o.OrderManagement.View(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgDataFound, v))
}
