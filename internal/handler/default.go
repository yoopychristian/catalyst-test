package handler

import (
	"test-service/domain"

	"github.com/gofiber/fiber/v2"
)

func DefaultHandler(r fiber.Router, p string) {
	r.Get(p+"/ping", ping)
}

// Ping godoc
// @Summary Show the status of server.
// @Description Ping the status of server, should be respond fastly.
// @Tags Default
// @Accept */*
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /ping [get]
func ping(ctx *fiber.Ctx) error {
	return ctx.JSON(domain.DefaultSuccessResponse("succeeded ping with pong!", "pong"))
}
