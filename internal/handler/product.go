package handler

import (
	"test-service/domain"
	"test-service/internal"
	"test-service/internal/config"
	"test-service/internal/usecase"

	"github.com/gofiber/fiber/v2"
)

type Product struct {
	usecase.ProductManagement
}

func newProductResource(h Product) *Product {
	return &h
}

func ProductHandler(r fiber.Router, h Product) {
	handler := newProductResource(h)
	r.Post("/product", handler.add)
	r.Get("/product", handler.viewProduct)
	r.Get("/product/brand", handler.viewBrand)
}

// @Tags Product Management APIs
// Add Product API
// @Summary Add Product API
// @Schemes
// @Accept json
// @Param Authorization header string true "Your Token to Access" default(Bearer )
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param request body domain.AddProductRequest true "add Product"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /v1/product [post]
func (p *Product) add(ctx *fiber.Ctx) error {
	inp := domain.AddProductRequest{}
	if err := ctx.BodyParser(&inp); err != nil {
		return ctx.Status(404).JSON(err)
	}
	inp.SessionRequest = config.SessionBuild(ctx)
	v, ex := p.ProductManagement.Add(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgSubmit, v))
}

// @Tags Product Management APIs
// API Get Product
// @Summary Get Product API
// @Schemes
// @Accept json
// @Param Authorization header string true "Your Token to Access" default(Bearer )
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param Payload query domain.GetQueryRequest true "Query By Id Product"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /v1/product [get]
func (p *Product) viewProduct(ctx *fiber.Ctx) error {
	inp := domain.GetQueryRequest{}
	if err := ctx.QueryParser(&inp); err != nil {
		return ctx.Status(400).JSON(domain.BadPayloadMeta())
	}
	inp.SessionRequest = config.SessionBuild(ctx)
	inp.Id = ctx.Query("id")
	v, ex := p.ProductManagement.View(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgDataFound, v))
}

// @Tags Product Management APIs
// API Get Product by Brand
// @Summary Get Product by Brand API
// @Schemes
// @Accept json
// @Param Authorization header string true "Your Token to Access" default(Bearer )
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param Payload query domain.GetQueryRequest true "Query By Id Brand"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /v1/product/brand [get]
func (p *Product) viewBrand(ctx *fiber.Ctx) error {
	inp := domain.GetQueryRequest{}
	if err := ctx.QueryParser(&inp); err != nil {
		return ctx.Status(400).JSON(domain.BadPayloadMeta())
	}
	inp.SessionRequest = config.SessionBuild(ctx)
	inp.Id = ctx.Query("id")
	v, ex := p.ProductManagement.ViewByBrand(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgDataFound, v))
}
