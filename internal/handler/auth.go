package handler

import (
	"test-service/domain"
	"test-service/internal"
	"test-service/internal/usecase"

	"github.com/gofiber/fiber/v2"
)

type User struct {
	usecase.UserManagement
}

func newUserResource(h User) *User {
	return &h
}

func UserHandler(r fiber.Router, h User) {
	handler := newUserResource(h)
	r.Post("/users", handler.add)
	r.Post("/users/auth", handler.auth)
}

// @Tags User Management APIs
// Registration API
// @Summary Registration API
// @Schemes
// @Accept json
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param request body domain.SignupRequest true "registration user"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /users [post]
func (u *User) add(ctx *fiber.Ctx) error {
	inp := domain.SignupRequest{}
	if err := ctx.BodyParser(&inp); err != nil {
		return ctx.Status(404).JSON(err)
	}
	v, ex := u.UserManagement.Reg(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrMsgInvalidOwnPerson {
		return ctx.Status(400).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgSubmit, v))
}

// @Tags User Management APIs
// Authorization API
// @Summary Authorization API
// @Schemes
// @Accept json
// @Param x-client-channel  header string true "Client Platform Channel" Enums(WEBADMIN,APPTRAVELLER,THIRDPARTY)
// @Param x-client-deviceid  header string true "Client Platform Device ID"
// @Param x-client-language header string true "Client Platform Lang" Enums(EN,ID)
// @Param x-client-os  header string true "Client Platform OS"
// @Param x-client-reqts  header string false "Client Platform Request Timestamp"
// @Param x-client-trxid  header string false "Client Platform Transaction ID"
// @Param x-client-version  header string true "Client Platform Version"
// @Param request body domain.LoginRequest true "login user"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /users/auth [post]
func (u *User) auth(ctx *fiber.Ctx) error {
	inp := domain.LoginRequest{}
	if err := ctx.BodyParser(&inp); err != nil {
		return ctx.Status(404).JSON(err)
	}
	v, ex := u.UserManagement.Auth(&inp)
	if ex != nil && ex.ErrorCode == internal.ErrCodeDataNotFound {
		return ctx.Status(200).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrCodeSomethingWrong {
		return ctx.Status(500).JSON(domain.BusinessErrorResponse(ex))
	} else if ex != nil && ex.ErrorCode == internal.ErrMsgInvalidOwnPerson {
		return ctx.Status(400).JSON(domain.BusinessErrorResponse(ex))
	}
	return ctx.JSON(domain.DefaultSuccessResponse(internal.SuccessMsgSubmit, v))
}
