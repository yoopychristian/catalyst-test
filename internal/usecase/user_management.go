package usecase

import (
	"time"

	"test-service/domain"

	"test-service/internal"
	"test-service/internal/config"
	"test-service/internal/repository"

	"github.com/golang-jwt/jwt"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

type UserManagement struct {
	Dao          repository.User
	Logger       *zap.Logger
	TimeDuration string
}

func NewUserManagement(p UserManagement) *UserManagement {
	return &p
}

func (u *UserManagement) Reg(req *domain.SignupRequest) (trx *domain.TransactionResponse, ex *domain.BusinessError) {
	if req.Username == "" || req.Fullname == "" || req.Email == "" || req.Password == "" {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	// save this info in the database
	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	user := &domain.User{
		Fullname: req.Fullname,
		Username: req.Username,
		Email:    req.Email,
		Password: string(hash),
	}

	ers := u.Dao.Reg(*user)
	if ers != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	_, exp, err := u.createJWTToken(*user)
	if err != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}
	return &domain.TransactionResponse{
		TransactionTimestamp: uint(time.Now().Unix()),
		TransactionId:        config.GenerateTransactionId(string(exp) + internal.DefaultTrxId),
	}, nil
}

func (u *UserManagement) Auth(req *domain.LoginRequest) (trx *domain.TransactionResponseAuthorization, ex *domain.BusinessError) {
	if req.Email == "" || req.Password == "" {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	has, data, ers := u.Dao.Auth(*req)
	if ers != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	if !has {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	if err := bcrypt.CompareHashAndPassword([]byte(data.Password), []byte(req.Password)); err != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	token, exp, err := u.createJWTToken(*data)
	if err != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	return &domain.TransactionResponseAuthorization{
		Token:                token,
		TransactionTimestamp: uint(time.Now().Unix()),
		TransactionId:        config.GenerateTransactionId(string(exp) + internal.DefaultTrxId),
	}, nil
}

func (u *UserManagement) createJWTToken(user domain.User) (string, int64, error) {
	exp := time.Now().Add(time.Minute * 60).Unix()
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = user.Username
	claims["user_id"] = user.Id
	claims["exp"] = exp
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return "", 0, err
	}

	return t, exp, nil
}
