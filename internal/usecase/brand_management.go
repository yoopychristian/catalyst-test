package usecase

import (
	"database/sql"
	"time"

	"test-service/db"
	"test-service/domain"

	"test-service/internal"
	"test-service/internal/config"
	"test-service/internal/repository"

	"go.uber.org/zap"
)

type BrandManagement struct {
	Dao          repository.Brand
	Logger       *zap.Logger
	Cacher       db.Cacher
	TimeDuration string
}

func NewBrandManagement(p BrandManagement) *BrandManagement {
	return &p
}

func (p *BrandManagement) Add(inp *domain.AddBrandRequest) (trx *domain.TransactionResponseWithId, ex *domain.BusinessError) {
	brandId, err := p.Dao.Add(domain.Brand{
		Brand:     sql.NullString{Valid: true, String: inp.Brand},
		ShortDesc: sql.NullString{Valid: true, String: inp.ShortDesc},
		BaseEntity: domain.BaseEntity{
			CreatedBy: sql.NullString{Valid: true, String: inp.Username},
		},
	})
	if err != nil {
		p.Logger.Info("failed to add brand database", zap.Any("error", err))
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	return &domain.TransactionResponseWithId{
		Id:                   brandId,
		TransactionTimestamp: uint(time.Now().Unix()),
		TransactionId:        config.GenerateTransactionId(inp.Msisdn + internal.DefaultTrxId),
	}, nil
}
