package usecase

import (
	"database/sql"
	"strconv"
	"time"

	"test-service/db"
	"test-service/domain"

	"test-service/internal"
	"test-service/internal/config"
	"test-service/internal/repository"

	"github.com/shopspring/decimal"
	"go.uber.org/zap"
)

type ProductManagement struct {
	Dao          repository.Product
	Logger       *zap.Logger
	Cacher       db.Cacher
	TimeDuration string
}

func NewProductManagement(p ProductManagement) *ProductManagement {
	return &p
}

func (p *ProductManagement) Add(inp *domain.AddProductRequest) (trx *domain.TransactionResponseWithId, ex *domain.BusinessError) {
	ProductId, err := p.Dao.Add(domain.Product{
		BrandId:       sql.NullInt64{Valid: true, Int64: inp.BrandId},
		ProductName:   sql.NullString{Valid: true, String: inp.ProductName},
		OriginalPrice: decimal.NullDecimal{Valid: true, Decimal: decimal.New(inp.OriginalPrice, 0)},
		ShortDesc:     sql.NullString{Valid: true, String: inp.ShortDesc},
		BaseEntity: domain.BaseEntity{
			CreatedBy: sql.NullString{Valid: true, String: inp.Username},
		},
	})
	if err != nil {
		p.Logger.Info("failed to add Product database", zap.Any("error", err))
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	return &domain.TransactionResponseWithId{
		Id:                   ProductId,
		TransactionTimestamp: uint(time.Now().Unix()),
		TransactionId:        config.GenerateTransactionId(inp.Msisdn + internal.DefaultTrxId),
	}, nil
}

func (p *ProductManagement) View(inp *domain.GetQueryRequest) (res *domain.ProductDetailProjection, ex *domain.BusinessError) {
	pid, _ := strconv.ParseInt(inp.Id, 10, 64)
	res, err := p.Dao.Get(inp.Username, pid)
	if err != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeDataNotFound, ErrorMessage: internal.ErrMsgDataNotFound}
	}

	return res, nil
}

func (p *ProductManagement) ViewByBrand(inp *domain.GetQueryRequest) (res []*domain.ProductBrandResponse, ex *domain.BusinessError) {
	bid, _ := strconv.ParseInt(inp.Id, 10, 64)
	res, err := p.Dao.GetByBrand(inp.Username, bid)
	if err != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeDataNotFound, ErrorMessage: internal.ErrMsgDataNotFound}
	}

	return res, nil
}
