package usecase

import (
	"database/sql"
	"strconv"
	"time"

	"test-service/db"
	"test-service/domain"

	"test-service/internal"
	"test-service/internal/config"
	"test-service/internal/repository"

	"github.com/shopspring/decimal"
	"go.uber.org/zap"
)

type OrderManagement struct {
	Dao          repository.Order
	Logger       *zap.Logger
	Cacher       db.Cacher
	TimeDuration string
}

func NewOrderManagement(p OrderManagement) *OrderManagement {
	return &p
}

func (o *OrderManagement) Add(inp *domain.AddOrderRequest) (trx *domain.TransactionResponseWithIdPrice, ex *domain.BusinessError) {
	price, err := o.Dao.GetPrice(inp.ProductId)
	if err != nil {
		o.Logger.Info("failed to get price database", zap.Any("error", err))
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	totalPrice := price * inp.Qty

	OrderId, err := o.Dao.Add(domain.Order{
		ProductId:  sql.NullInt64{Valid: true, Int64: inp.ProductId},
		Qty:        sql.NullInt64{Valid: true, Int64: inp.Qty},
		Notes:      sql.NullString{Valid: true, String: inp.Notes},
		TotalPrice: decimal.NullDecimal{Valid: true, Decimal: decimal.New(totalPrice, 0)},
		BaseEntity: domain.BaseEntity{
			CreatedBy: sql.NullString{Valid: true, String: inp.Username},
		},
	})
	if err != nil {
		o.Logger.Info("failed to add Order database", zap.Any("error", err))
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeSomethingWrong, ErrorMessage: internal.ErrMsgSomethingWrong}
	}

	return &domain.TransactionResponseWithIdPrice{
		Id:                   OrderId,
		Price:                totalPrice,
		TransactionTimestamp: uint(time.Now().Unix()),
		TransactionId:        config.GenerateTransactionId(inp.Msisdn + internal.DefaultTrxId),
	}, nil
}

func (o *OrderManagement) View(inp *domain.GetQueryRequest) (res *domain.OrderDetailProjection, ex *domain.BusinessError) {
	pid, _ := strconv.ParseInt(inp.Id, 10, 64)
	res, err := o.Dao.Get(inp.Username, pid)
	if err != nil {
		return nil, &domain.BusinessError{ErrorCode: internal.ErrCodeDataNotFound, ErrorMessage: internal.ErrMsgDataNotFound}
	}

	return res, nil
}
