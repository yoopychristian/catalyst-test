package repository

import (
	"context"

	"test-service/db"
	"test-service/domain"
	"test-service/internal/config"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"go.uber.org/zap"
)

type Order struct {
	db.PgPool
	Logger *zap.Logger
}

func NewOrder(repo Order) *Order {
	return &repo
}

func (p *Order) Add(v domain.Order) (OrderId int64, ex *domain.TechnicalError) {
	tx, err := p.Pool.BeginTx(context.Background(), pgx.TxOptions{IsoLevel: pgx.Serializable})
	if err != nil {
		p.Logger.Error("begin transaction save person failed", zap.Error(err))
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		err := tx.Rollback(ctx)
		if err != nil {
			p.Logger.Error("rollback transaction save person failed", zap.Error(err))
		}
	}(tx, context.Background())

	err = tx.QueryRow(context.Background(), "INSERT INTO orders "+
		" (product_id, qty, total_price, notes, created_by,  created_date, status, is_deleted) "+
		" VALUES ($1, $2, $3, $4, $5, NOW(), 1, false) RETURNING ID", v.ProductId, v.Qty, v.TotalPrice, v.Notes, v.BaseEntity.CreatedBy).Scan(&OrderId)
	if err != nil {
		p.Logger.Error("begin transaction save person failed", zap.Error(err))
	}

	if err = tx.Commit(context.Background()); err != nil {
		p.Logger.Panic("transaction save order failed", zap.Error(err))
	}
	return OrderId, nil
}

func (p *Order) Get(u string, OrderId int64) (res *domain.OrderDetailProjection, ex *domain.TechnicalError) {
	v := domain.OrderDetailProjection{}
	query, _ := p.Pool.Query(context.Background(),
		` select jsonb_build_object('id', o.id, 'product_name', p.product_name, 'qty', o.qty, 'total_price', o.total_price, 'notes', o.notes) orders
		FROM orders o
		left join products p on o.product_id = p.id
		where o.created_by = $1 AND o.is_deleted = false AND p.status = 1 and o.id = $2
		group by o.id, p.product_name, o.qty, o.total_price, o.notes ; `, u, OrderId)
	err := pgxscan.ScanOne(&v, query)
	if err != nil {
		return nil, config.Exception("failed to find detail Order by session", err, zap.String("username", u), p.Logger)
	}
	return &v, nil
}

func (o *Order) GetPrice(pid int64) (int64, *domain.TechnicalError) {
	var price int64
	vrow, err := o.Pool.Query(context.Background(), `SELECT original_price from products WHERE id=$1 AND is_deleted = false`,
		pid)
	if err != nil {
		return 0, config.Exception("failed to get price", err, zap.Int64("", pid), o.Logger)
	}
	err = pgxscan.ScanOne(&price, vrow)
	if err != nil {
		return 0, config.Exception("failed to map count get price", err, zap.Int64("", pid), o.Logger)
	}
	return price, nil
}
