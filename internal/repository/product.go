package repository

import (
	"context"

	"test-service/db"
	"test-service/domain"
	"test-service/internal/config"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"go.uber.org/zap"
)

type Product struct {
	db.PgPool
	Logger *zap.Logger
}

func NewProduct(repo Product) *Product {
	return &repo
}

func (p *Product) Add(v domain.Product) (productId int64, ex *domain.TechnicalError) {
	tx, err := p.Pool.BeginTx(context.Background(), pgx.TxOptions{IsoLevel: pgx.Serializable})
	if err != nil {
		p.Logger.Error("begin transaction save person failed", zap.Error(err))
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		err := tx.Rollback(ctx)
		if err != nil {
			p.Logger.Error("rollback transaction save person failed", zap.Error(err))
		}
	}(tx, context.Background())

	err = tx.QueryRow(context.Background(), "INSERT INTO products "+
		" (brand_id, product_name, promo_status, original_price, created_by, short_desc, created_date, status, is_deleted) "+
		" VALUES ($1, $2, false, $3, $4, $5, NOW(), 1, false) RETURNING ID", v.BrandId, v.ProductName, v.OriginalPrice, v.BaseEntity.CreatedBy, v.ShortDesc).Scan(&productId)
	if err != nil {
		p.Logger.Error("begin transaction save person failed", zap.Error(err))
	}

	if err = tx.Commit(context.Background()); err != nil {
		p.Logger.Panic("transaction save person failed", zap.Error(err))
	}
	return productId, nil
}

func (p *Product) Get(u string, productId int64) (res *domain.ProductDetailProjection, ex *domain.TechnicalError) {
	v := domain.ProductDetailProjection{}
	query, _ := p.Pool.Query(context.Background(),
		` select jsonb_build_object('id', p.id, 'brand_name', b.brand, 'product_name', p.product_name, 'promo_status', p.promo_status, 'original_price', p.original_price, 'short_desc', p.short_desc, 'status', p.status) product
		 FROM products p
		 left join brands b on p.brand_id = b.id
		 where p.created_by = $1 AND p.is_deleted = false AND p.status = 1 and p.id = $2
		 group by p.id, p.product_name, p.status, b.brand, p.promo_status, p.original_price, p.short_desc, p.status ; `, u, productId)
	err := pgxscan.ScanOne(&v, query)
	if err != nil {
		return nil, config.Exception("failed to find detail product by session", err, zap.String("username", u), p.Logger)
	}
	return &v, nil
}

func (p *Product) GetByBrand(u string, brandId int64) (res []*domain.ProductBrandResponse, ex *domain.TechnicalError) {
	query, _ := p.Pool.Query(context.Background(),
		` select p.id, b.brand as brand_name,  p.product_name,  p.promo_status,  p.original_price,  p.short_desc, p.status
		FROM products p
		left join brands b on p.brand_id = b.id
		where p.created_by = $1 AND p.is_deleted = false AND p.status = 1 and p.brand_id = $2
		group by p.id, p.product_name, p.status, b.brand, p.promo_status, p.original_price, p.short_desc, p.status ; `, u, brandId)
	err := pgxscan.ScanAll(&res, query)
	if err != nil {
		return nil, config.Exception("failed to find detail product by brand by session", err, zap.String("username", u), p.Logger)
	}
	return res, nil
}
