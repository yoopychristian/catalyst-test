package repository

import (
	"test-service/data"
	"test-service/db"
	"test-service/domain"

	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

type User struct {
	db.PgXorm
	Logger *zap.Logger
}

func NewUser(repo User) *User {
	return &repo
}

func (u *User) Reg(v domain.User) (ex *domain.TechnicalError) {
	engine, err := data.CreateDBEngine()
	if err != nil {
		panic(err)
	}

	_, err = engine.Insert(v)
	if err != nil {
		u.Logger.Error("rollback transaction save user failed", zap.Error(err))
	}

	return nil
}

func (u *User) Auth(v domain.LoginRequest) (a bool, c *domain.User, ex *domain.TechnicalError) {
	engine, err := data.CreateDBEngine()
	if err != nil {
		panic(err)
	}

	user := new(domain.User)
	has, err := engine.Where("email = ?", v.Email).Desc("id").Get(user)
	if err != nil {
		u.Logger.Error("rollback transaction get user failed", zap.Error(err))
	}

	return has, user, nil
}
