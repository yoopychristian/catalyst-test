package repository

import (
	"context"

	"test-service/db"
	"test-service/domain"

	"github.com/jackc/pgx/v4"
	"go.uber.org/zap"
)

type Brand struct {
	db.PgPool
	Logger *zap.Logger
}

func NewBrand(repo Brand) *Brand {
	return &repo
}

func (p *Brand) Add(v domain.Brand) (brandId int64, ex *domain.TechnicalError) {
	tx, err := p.Pool.BeginTx(context.Background(), pgx.TxOptions{IsoLevel: pgx.Serializable})
	if err != nil {
		p.Logger.Error("begin transaction save person failed", zap.Error(err))
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		err := tx.Rollback(ctx)
		if err != nil {
			p.Logger.Error("rollback transaction save person failed", zap.Error(err))
		}
	}(tx, context.Background())

	err = tx.QueryRow(context.Background(), "INSERT INTO brands "+
		" (brand, short_desc, created_by, created_date, status, is_deleted) "+
		" VALUES ($1, $2, $3, NOW(), 1, false) RETURNING ID", v.Brand, v.ShortDesc, v.BaseEntity.CreatedBy).Scan(&brandId)
	if err != nil {
		p.Logger.Error("begin transaction save person failed", zap.Error(err))
	}

	if err = tx.Commit(context.Background()); err != nil {
		p.Logger.Panic("transaction save person failed", zap.Error(err))
	}
	return brandId, nil
}
