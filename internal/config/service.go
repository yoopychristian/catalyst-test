package config

import (
	"time"

	"test-service/domain"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type HandlerConfig struct {
	Logger *zap.Logger
}

func HandlerLog(c HandlerConfig) fiber.Handler {
	logger := c.Logger
	return func(ctx *fiber.Ctx) error {
		start := time.Now().UnixMilli()
		c := domain.ContextLog{
			Api:             ctx.BaseURL(),
			Method:          ctx.Method(),
			MediaType:       ctx.Accepts(),
			IncomingReqTime: start,
		}
		logger.Info("incoming request", zap.Any("", c))
		return ctx.Next()
	}
}
