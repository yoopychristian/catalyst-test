package config

import (
	"os"
	"strconv"
	"time"

	"test-service/domain"
	"test-service/internal"

	"github.com/gofiber/fiber/v2"
	"github.com/gojek/heimdall"
	"github.com/gojek/heimdall/v7/httpclient"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Base struct {
	Logger *zap.Logger
}

func NewBase() Base {
	log := Log(false)
	err := Env(log)
	if err != nil {
		log.Fatal("error to load config", zap.Any("", &err))
	}
	return Base{
		Logger: log,
	}
}

type RestAdaptor struct {
	BackoffInterval   time.Duration
	MaxJitterInterval time.Duration
	Timeout           time.Duration
	EsbBackofficeKey  string
	GmapApiKey        string
}

func (r *RestAdaptor) Client() *httpclient.Client {
	retrier := heimdall.NewRetrier(heimdall.NewConstantBackoff(r.BackoffInterval, r.MaxJitterInterval))
	return httpclient.NewClient(httpclient.WithHTTPTimeout(r.Timeout), httpclient.WithRetrier(retrier), httpclient.WithRetryCount(2))
}

func Log(prod bool) (z *zap.Logger) {
	var c zap.Config
	if prod {
		c = zap.NewProductionConfig()
		c.DisableStacktrace = true

	} else {
		c = zap.NewDevelopmentConfig()
		c.DisableStacktrace = false
	}
	c.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	z, _ = c.Build()
	return z

}

func Env(logger *zap.Logger) *domain.TechnicalError {
	if _, err := os.Stat(".env"); err == nil {
		err := godotenv.Load()
		if err != nil {
			logger.Error("failed to load configuration from .env file", zap.Error(err))
			return &domain.TechnicalError{
				Exception: err.Error(),
			}
		}
	} else {
		logger.Info("no configuration from .env")
	}
	return nil
}

func Exception(msg string, err error, d zap.Field, logger *zap.Logger) *domain.TechnicalError {
	e := &domain.TechnicalError{
		Exception: err.Error(),
		Occurred:  time.Now().Unix(),
		Ticket:    uuid.New().String(),
	}
	logger.Error(msg, zap.Any("", e), d)
	return e
}

func GenerateTransactionId(identifier string) string {
	return "TEST01" + time.Now().Format("060102150405")
}

func PageBuild(iLimit int, iStart int) (limit int, start int) {
	if iLimit == 0 || iLimit > 100 {
		limit = 10
	} else {
		limit = iLimit
	}

	if iStart <= 1 {
		iStart = 0
	} else {
		iStart = iStart - 1
	}
	return limit, iStart * limit
}

func SessionsBuild(id string, uname string, ctx *fiber.Ctx) (s domain.SessionResponse) {
	ctx.Request().Header.Add(internal.HeaderSessionUsername, uname)
	ctx.Request().Header.Add(internal.HeaderSessionId, id)
	ctx.Request().Header.Add(internal.HeaderSessionEmail, s.Email)
	ctx.Request().Header.Add(internal.HeaderSessionMsisdn, s.Msisdn)
	ctx.Request().Header.Add(internal.HeaderSessionExpired, strconv.FormatUint(s.Expired, 10))
	ctx.Request().Header.Add(internal.HeaderSessionRole, s.Role)
	ctx.Request().Header.Add(internal.HeaderSessionStatus, strconv.FormatInt(int64(s.Status), 10))
	return s
}
