package config

import (
	"fmt"
	"strings"
	"test-service/domain"
	"test-service/internal"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"go.uber.org/zap"
)

type Middleware struct {
	Logger *zap.Logger
}

func HandlerAuth(m Middleware) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		auth := ctx.Get(internal.HeaderAuthorization)
		split := strings.Split(auth, "Bearer ")
		channel := ctx.Get(internal.HeaderClientChannel)

		var uname string
		var userId float64

		if channel == "WEBADMIN" {
			if len(split) == 1 {
				return ctx.Status(401).JSON(domain.Response{
					Meta: domain.Meta{
						Code:    internal.ErrCodeUnauthorized,
						Message: internal.ErrMsgUnauthorized,
					},
				})
			}
			res, err := JwtDecoder(split[1])
			if err != nil {
				m.Logger.Error("failed to get expired jwt", zap.Any("", err))
				return ctx.Status(401).JSON(domain.Response{
					Meta: domain.Meta{
						Code:    internal.ErrCodeUnauthorized,
						Message: internal.ErrMsgUnauthorized,
					},
				})
			}
			uname = res["username"].(string)
			userId = res["user_id"].(float64)
		}

		SessionsBuild(fmt.Sprintf("%v", userId), uname, ctx)
		return ctx.Next()
	}
}

func SessionBuild(ctx *fiber.Ctx) domain.SessionRequest {
	return domain.SessionRequest{
		Username: ctx.Get(internal.HeaderSessionUsername),
		Email:    ctx.Get(internal.HeaderSessionEmail),
		Msisdn:   ctx.Get(internal.HeaderSessionMsisdn),
		Id:       ctx.Get(internal.HeaderSessionId),
		ContextRequest: domain.ContextRequest{
			Channel:       ctx.Get(internal.HeaderClientChannel),
			DeviceId:      ctx.Get(internal.HeaderClientDeviceId),
			Authorization: ctx.Get(internal.HeaderAuthorization),
		},
	}
}

func JwtDecoder(tokenString string) (jwt.MapClaims, error) {
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte("secret"), nil
	})
	if err != nil {
		return nil, err
	}

	return claims, nil
}
