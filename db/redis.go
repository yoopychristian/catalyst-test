package db

import (
	"test-service/domain"
	"test-service/internal/config"
	"time"

	"github.com/go-redis/redis"
	"go.uber.org/zap"
)

type (
	RedisOptions struct {
		Addr   string
		Addrs  []string
		Passwd string
		Index  int
		Pool   int
		Idle   int
		Logger *zap.Logger
	}

	singleRedis struct {
		logger *zap.Logger
		cache  *redis.Client
	}
)

func NewRedis(o *RedisOptions) Cacher {
	return &singleRedis{
		logger: o.Logger,
		cache: redis.NewClient(&redis.Options{
			Addr:         o.Addr,
			Password:     o.Passwd,
			DB:           o.Index,
			PoolSize:     o.Pool,
			MinIdleConns: o.Idle,
		}),
	}
}

type Cacher interface {
	Set(k string, p string, v interface{}, d time.Duration) *domain.TechnicalError
	Delete(k string, p string) *domain.TechnicalError
	Get(k string, p string) (v string, e *domain.TechnicalError)
	Ttl(k string, p string) (t time.Duration, e *domain.TechnicalError)
}

func (r singleRedis) Set(k string, p string, v interface{}, d time.Duration) *domain.TechnicalError {
	r.cache.Del(k + ":" + p)
	if d != 0*time.Second {
		_, err := r.cache.SetNX(k+":"+p, v, d).Result()
		if err != nil {
			return config.Exception("failed on setnx ops", err, zap.String("keypair", k+":"+p), r.logger)
		}
	} else {
		_, err := r.cache.Set(k+":"+p, v, 0).Result()
		if err != nil {
			return config.Exception("failed on set ops", err, zap.String("keypair", k+":"+p), r.logger)
		}
	}
	return nil
}

func (r singleRedis) Delete(k string, p string) *domain.TechnicalError {
	if cmd := r.cache.Del(k + ":" + p); cmd.Err() != nil {
		return config.Exception("failed on delete ops", cmd.Err(), zap.String("keypair", k+":"+p), r.logger)
	}
	return nil
}

func (r singleRedis) Get(k string, p string) (string, *domain.TechnicalError) {
	v, err := r.cache.Get(k + ":" + p).Result()
	if err != nil {
		return v, config.Exception("failed on get ops", err, zap.String("keypair", k+":"+p), r.logger)
	}
	return v, nil
}

func (r singleRedis) Ttl(k string, p string) (t time.Duration, e *domain.TechnicalError) {
	cmd := r.cache.TTL(k + ":" + p)
	if cmd.Err() != nil {
		return t, config.Exception("failed on TTL ops", cmd.Err(), zap.String("keypair", k+":"+p), r.logger)
	} else {
		return cmd.Val(), nil
	}
}
