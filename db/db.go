package db

import (
	"context"
	"strings"

	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
	"xorm.io/xorm"
)

type PgOptions struct {
	Host    string
	Port    string
	User    string
	Passwd  string
	Schema  string
	Options *string
	Logger  *zap.Logger
}

type PgPool struct {
	Pool *pgxpool.Pool
}

type PgXorm struct {
	Engine *xorm.Engine
}

func NewPgPool(pg *PgOptions) *PgPool {
	url := "postgres://{{username}}:{{password}}@{{host}}:{{port}}/{{schema}}"
	url = strings.Replace(url, "{{host}}", pg.Host, -1)
	url = strings.Replace(url, "{{port}}", pg.Port, -1)
	url = strings.Replace(url, "{{username}}", pg.User, -1)
	url = strings.Replace(url, "{{password}}", pg.Passwd, -1)
	url = strings.Replace(url, "{{schema}}", pg.Schema, -1)
	if pg.Options != nil {
		url += "?" + *pg.Options
	}
	pool, err := pgxpool.Connect(context.Background(), url)
	if err != nil {
		pg.Logger.Fatal("failed to settle postgres connection", zap.Error(err))
	}
	return &PgPool{Pool: pool}
}
